# IBM i Dash
Welcome!  This is an open source project that aims to make it easy to create dashboard pages for IBM i purposes.

##Please fork and add your own dashboard page!

![ibmidash_logo.png](https://bitbucket.org/repo/LjEMEz/images/345524619-ibmidash_logo.png)

![2015-06-01_18-38-07.png](https://bitbucket.org/repo/LjEMEz/images/613510403-2015-06-01_18-38-07.png)

![2015-06-01_18-50-24.png](https://bitbucket.org/repo/LjEMEz/images/65460062-2015-06-01_18-50-24.png)